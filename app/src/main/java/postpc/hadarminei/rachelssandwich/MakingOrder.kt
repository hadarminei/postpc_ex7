package postpc.hadarminei.rachelssandwich

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer

class MakingOrder : AppCompatActivity() {
    private lateinit var ordersApp : OrdersApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ordersApp = this.applicationContext as OrdersApplication
        setContentView(R.layout.making_screen)

        //set observer to the liveData
        ordersApp.orderLiveData.observe(this, Observer { sandwich: Sandwich ->
            if (sandwich.status == "ready"){
                val intent = Intent(this, ReadyOrder::class.java)
                startActivity(intent)
            }
        })
    }
}