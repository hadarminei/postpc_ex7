package postpc.hadarminei.rachelssandwich
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer

class MainActivity : AppCompatActivity() {

    private lateinit var orderApp: OrdersApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderApp = this.applicationContext as OrdersApplication
        setContentView(R.layout.order_screen)
        if (orderApp.id.isNullOrEmpty()) {
            val intent = Intent(this, NewOrder::class.java)
            startActivity(intent)
        } else {
            orderApp.orderLiveData.observe(this, Observer { sandwich: Sandwich? ->
                if(sandwich == null) {
                    Log.e("fix", "bug")
                }
                if (sandwich != null) {
                    openActivityBySandwichStatus(sandwich)
                }
            })
            orderApp.orderLiveData.value?.let { openActivityBySandwichStatus(it) }
        }
    }

    private fun openActivityBySandwichStatus(sandwich: Sandwich) {
        if (sandwich.status == "waiting") {
            val intent = Intent(this, EditOrder::class.java)
            startActivity(intent)
        } else if (sandwich.status == "in_progress") {
            val intent = Intent(this, MakingOrder::class.java)
            startActivity(intent)
        } else if (sandwich.status == "ready") {
            val intent = Intent(this, ReadyOrder::class.java)
            startActivity(intent)
        } else {
            Log.d("LiveData", "This sandwich object has no valid status")
        }
    }
}