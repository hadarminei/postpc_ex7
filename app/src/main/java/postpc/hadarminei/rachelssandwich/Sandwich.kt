package postpc.hadarminei.rachelssandwich

import java.util.*

data class Sandwich (
    var id: String = UUID.randomUUID().toString(),
    var name: String = "",
    var pickles: Int = 0,
    var hummus: Boolean = true,
    var tahini: Boolean = true,
    var comments: String = "",
    var status: String = "waiting"
)