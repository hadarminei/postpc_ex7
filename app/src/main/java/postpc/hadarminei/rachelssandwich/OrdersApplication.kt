package postpc.hadarminei.rachelssandwich

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseException
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore

class OrdersApplication : Application() {
    lateinit var sp : SharedPreferences
    private lateinit var db : FirebaseFirestore
    var id: String? = null
    var name: String? = null

    private val sandwichData: MutableLiveData<Sandwich> = MutableLiveData()
    val orderLiveData: LiveData<Sandwich>
        get() = sandwichData

    override fun onCreate() {
        super.onCreate()
        sp = this.getSharedPreferences("sandwich_data", Context.MODE_PRIVATE)
        FirebaseApp.initializeApp(this)
        db = FirebaseFirestore.getInstance()

        initializeFromSp()
        if (id!!.isNotEmpty()){
            initializeObserver()
        }
    }


    private fun initializeFromSp() {
        id = sp.getString("id", "").toString()
        name = sp.getString("name", "").toString()
        if (id!!.isNotEmpty()){
            db.collection("orders").document(id.toString()).get().addOnSuccessListener {result ->
                Log.e("res", result.data.toString())
                Log.e("id", id.toString())
                sandwichData.value = result?.toObject(Sandwich::class.java)
            }
        }
    }

    public fun updateAndUpload(sandwich: Sandwich){
        if (id.isNullOrEmpty()){
            id = sandwich.id
            initializeObserver()
        }
        if (name.isNullOrEmpty()){
            name = sandwich.name
        }
        db.collection("orders").document(sandwich.id).set(sandwich).addOnSuccessListener {
            Toast.makeText(this, "Your order was updated", Toast.LENGTH_SHORT).show()
        }
        sandwichData.value = sandwich
        val editor = sp.edit()
        editor.putString("id", id)
        editor.putString("name", sandwich.name)
        editor.apply()
    }

    private fun initializeObserver() {
        id?.let {
            if(it.isNotEmpty()){
                db.collection("orders").document(it).addSnapshotListener { result: DocumentSnapshot?, e: FirebaseException? ->
                    if (result != null) {
                        sandwichData.value = result.toObject(Sandwich::class.java)
                    }
                }
            }
        }
    }

    public fun deleteOrder(){
        if(id != null){
            Toast.makeText(this, "Your order has been deleted", Toast.LENGTH_SHORT).show()
            val editor = sp.edit()
            editor.remove("id")
            editor.clear()
            editor.apply()
            id = null
            sandwichData.value = null
        }
    }

}