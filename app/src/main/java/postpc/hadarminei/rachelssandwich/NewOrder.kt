package postpc.hadarminei.rachelssandwich

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.SwitchCompat
import androidx.constraintlayout.widget.ConstraintLayout

class NewOrder : AppCompatActivity(){

    private lateinit var ordersApp : OrdersApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ordersApp = this.applicationContext as OrdersApplication
        setContentView(R.layout.order_screen)

        val editName : EditText = findViewById(R.id.editName)
        val name : TextView = findViewById(R.id.name)
        var more_pickels : ImageButton = findViewById(R.id.plus)
        var less_pickles : ImageButton = findViewById(R.id.minus)
        var pickles : TextView = findViewById(R.id.pickles)
        val hummus : SwitchCompat = findViewById(R.id.hummus)
        val tahini : SwitchCompat = findViewById(R.id.tahini)
        val comments : EditText = findViewById(R.id.comments)
        val done : AppCompatButton = findViewById(R.id.doneButton)
        var num_pickles = 0
        var name_val = ""

        if(ordersApp.name != "") {
            editName.visibility = View.GONE
            name.text = ordersApp.name
            name_val = ordersApp.name.toString()
        }
        else {
            name.visibility = View.GONE
        }

        more_pickels.setOnClickListener() {
            if(num_pickles < 10) {
                num_pickles++
                pickles.text = num_pickles.toString()

                Log.e("pickles", num_pickles.toString())
            }
            if(num_pickles >= 1) {
                less_pickles.isEnabled = true;
            }
        }
        less_pickles.setOnClickListener() {
            if(num_pickles > 0) {
                num_pickles--
                pickles.text = num_pickles.toString()
            }
        }
        if(num_pickles == 10) {
            more_pickels.isEnabled = false
        }

        else if(num_pickles >= 0) {
            less_pickles.isEnabled = true
        }

        done.setOnClickListener() {
            if(ordersApp.name == "") {
                name_val = editName.text.toString()
            }
            updateOrder(num_pickles, tahini, hummus, comments, name_val)
            val intent = Intent(this, EditOrder::class.java)
            startActivity(intent)
        }
    }

    private fun updateOrder(
        pickles: Int,
        tahini: SwitchCompat,
        hummus: SwitchCompat,
        commentContent: EditText,
        name_val: String
    ) {
        val sandwich = Sandwich()
        sandwich.name = name_val
        sandwich.pickles = pickles.toString().toInt()
        sandwich.hummus = hummus.isChecked
        sandwich.tahini = tahini.isChecked
        sandwich.comments = commentContent.text.toString()
        ordersApp.updateAndUpload(sandwich)
    }
}