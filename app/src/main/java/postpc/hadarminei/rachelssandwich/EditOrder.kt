package postpc.hadarminei.rachelssandwich

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.Observer
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class EditOrder : AppCompatActivity(){
    private lateinit var ordersApp: OrdersApplication


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_screen)
        ordersApp = applicationContext as OrdersApplication
        val sandwich: Sandwich? = ordersApp.orderLiveData.value

        val name : TextView = findViewById(R.id.name)
        val more_pickels : ImageButton = findViewById(R.id.plus)
        val less_pickles : ImageButton = findViewById(R.id.minus)
        val pickles : TextView = findViewById(R.id.pickles)
        val hummus : SwitchCompat = findViewById(R.id.hummus)
        val tahini : SwitchCompat = findViewById(R.id.tahini)
        val comments : EditText = findViewById(R.id.comments)
        val editSave : AppCompatButton = findViewById(R.id.editButton)
        val delete : ImageView = findViewById(R.id.delete)
        setScreenData(sandwich, name, pickles, hummus, tahini, comments, less_pickles, more_pickels)
        var num_pickles = pickles.text.toString().toInt()

        more_pickels.setOnClickListener() {
            if(num_pickles < 10) {
                num_pickles++
                pickles.text = num_pickles.toString()
            }
            if(num_pickles >= 1) {
                less_pickles.isEnabled = true;
            }
        }
        less_pickles.setOnClickListener() {
            if(num_pickles > 0) {
                num_pickles--
                pickles.text = num_pickles.toString()
            }
        }
        if(num_pickles == 10) {
            more_pickels.isEnabled = false
        }

        else if(num_pickles >= 0) {
            less_pickles.isEnabled = true
        }

        delete.setOnClickListener {
            ordersApp.deleteOrder()
        }

        editSave.setOnClickListener {
            if(sandwich != null) {
                sandwich.pickles = num_pickles
                sandwich.hummus = hummus.isChecked
                sandwich.tahini = tahini.isChecked
                sandwich.comments = comments.text.toString()
                ordersApp.updateAndUpload(sandwich)
            }
        }

        ordersApp.orderLiveData.observe(this, Observer { it->
            if (it == null){
                val intent = Intent(this, NewOrder::class.java)
                startActivity(intent)
            }
            else if (it.status == "in_progress"){
                val intent = Intent(this, MakingOrder::class.java)
                startActivity(intent)
            }
        })
    }


    private fun setScreenData(
        sandwich: Sandwich?,
        name: TextView,
        pickles: TextView,
        hummus: SwitchCompat,
        tahini: SwitchCompat,
        comments: EditText,
        less_pickles: ImageButton,
        more_pickles: ImageButton
    ) {
        if (sandwich != null) {
            val nameSandwich = sandwich.name.toString() + "'s Sandwich"
            name.text = nameSandwich
            pickles.text = sandwich.pickles.toString()
            hummus.isChecked = sandwich.hummus
            tahini.isChecked = sandwich.tahini
        }

        val comment = sandwich?.comments
        comments.setText(comment)

        if (sandwich != null) {
            if(sandwich.pickles == 0) {
                less_pickles.isEnabled = false
            }
            else if(sandwich.pickles == 10) {
                more_pickles.isEnabled = false
            }
        }
    }
}