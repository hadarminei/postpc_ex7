package postpc.hadarminei.rachelssandwich

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton

class ReadyOrder : AppCompatActivity() {

    private lateinit var ordersApp : OrdersApplication

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ordersApp = this.applicationContext as OrdersApplication
        setContentView(R.layout.ready_screen)

        val gotMySandwich : AppCompatButton = findViewById(R.id.doneButton)

        gotMySandwich.setOnClickListener{
            val sandwich: Sandwich? = ordersApp.orderLiveData.value
            if (sandwich != null){
                ordersApp.deleteOrder()
                val intent = Intent(this, NewOrder::class.java)
                startActivity(intent)
            }
        }
    }
}